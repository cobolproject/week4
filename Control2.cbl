       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL2. 
       AUTHOR. SUWATINEE.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS HEX-NUMBER IS "0" THRU  "9" , "A" THRU  "F"
           CLASS REAL-NAME IS "A" THRU  "Z", "a" THRU "z" , "'" , SPACE.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM-IN  PIC X(4).
       01  NAME-IN PIC X(15).

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY  "ENTER A HEX NUMBER - " WITH NO ADVANCING 
           ACCEPT NUM-IN .
           IF NUM-IN IS HEX-NUMBER THEN
              DISPLAY NUM-IN " IS A HEX NUMBER"
           ELSE 
              DISPLAY NUM-IN  " IS NOT ALPHABETIC"
           END-IF 
           .

           DISPLAY "--------------------------------------"
           DISPLAY "ENTER A REAL NAME - " WITH NO ADVANCING 
           ACCEPT NAME-IN 
           IF NAME-IN IS REAL-NAME THEN 
              DISPLAY NAME-IN " IS A REAL NAME"
           ELSE 
           DISPLAY NAME-IN " IS NOT A REAL NAME"
           END-IF 
           .

           DISPLAY "ENTER A NAME - " WITH NO ADVANCING 
           ACCEPT NAME-IN 
           IF NAME-IN IS ALPHABETIC THEN
              DISPLAY NAME-IN " IS A NAME"
           ELSE 
              DISPLAY NAME-IN " IS NOT A NAME"
           END-IF 
           .
                  