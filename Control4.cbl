       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL4.
       AUTHOR. SUWATINEE.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT StudentFile ASSIGN TO "GRADE.DAT"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD StudentFile.
       01 StudentDetails.
           88 EndOfStudentFile VALUE HIGH-VALUE.
           05 StudentId   PIC X(8).
           05 StudentName PIC X(16).
           05 CourseCode  PIC X(8).
           05 Grade       PIC X(2).

       PROCEDURE DIVISION.
       Begin.
           OPEN INPUT StudentFile
           READ Studentfile
              AT END SET EndofStudentFile TO TRUE
           END-READ

           PERFORM UNTIL EndOfStudentFile
              READ Studentfile
                 AT END SET Endofstudentfile TO TRUE
              END-READ
              IF NOT Endofstudentfile THEN
                 DISPLAY StudentName SPACE StudentId SPACE CourseCode SP
      -               ACE Grade
              END-IF             
           END-PERFORM
           CLOSE StudentFile
           .
           